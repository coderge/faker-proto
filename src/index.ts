import restify, { Request, Response, Next } from 'restify';
import mockjs, { MockjsRandom } from 'mockjs';
import { initRepository } from './repository';
import {
  initPackageDefinition,
  getMethod,
  getMockTpl,
  getServiceDefination,
} from './mock';

type ConfigHandler = (
  req: restify.Request
) => {
  packageName: string;
  serviceName: string;
  methodName: string;
};
type ResponseHandler = (res: restify.Response, data: any) => void;
type MockHandlerOptions = {
  getConfigHandler: ConfigHandler;
  responseHandler?: ResponseHandler;
  hackMockTpl?: (
    key: string,
    type: string,
    random: MockjsRandom
  ) => string | (() => string);
};

const generateMockHandler = (
  options: MockHandlerOptions,
  repository: string
) => {
  const { getConfigHandler, responseHandler, hackMockTpl } = options;
  const packageDefinition = initPackageDefinition(repository);
  return async (req: Request, res: Response, next: Next) => {
    const { packageName, serviceName, methodName } = getConfigHandler(req);
    if (methodName) {
      const method = getMethod(
        packageDefinition,
        packageName,
        serviceName,
        methodName
      );
      const responseType = method?.responseType || '';
      const tpl = getMockTpl(
        packageDefinition,
        packageName,
        responseType,
        hackMockTpl
      );
      const mockData = mockjs.mock(tpl);
      responseHandler ? responseHandler(res, mockData) : res.json(mockData);
    } else {
      getServiceDefination(packageDefinition, packageName, serviceName);
      res.send('Hello');
    }
    return next();
  };
};

export const createServer = async (options: MockHandlerOptions) => {
  const { projectPath, repository, branch } = await initRepository();
  const server = restify.createServer();
  // CORS
  server.use((req: Request, res: Response, next: Next) => {
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Origin', req.headers.origin);
    next();
  });
  // HANDLER
  const handler = generateMockHandler(options, projectPath);
  // PAGE ROUTES
  server.get('/', (req: Request, res: Response, next: Next) => {
    res.end(
      `<h1>Mock server in running !</h1><p>${repository}</p><p>${branch}</p>`
    );
    next();
  });
  // API ROUTES
  server.opts('*', (req: Request, res: Response, next: Next) => {
    res.header(
      'Access-Control-Allow-Methods',
      req.headers['access-control-request-methods']
    );
    res.header(
      'Access-Control-Allow-Headers',
      req.headers['access-control-request-headers']
    );
    res.end();
    next();
  });
  server.get('*', handler);
  server.post('*', handler);
  return {
    start: (port: number = 3333) =>
      server.listen(port, () =>
        console.log('%s listening at %s', server.name, server.url)
      ),
  };
};
